<?php
include("db/db.php");

echo "
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Clients</title>
<link rel='stylesheet' href='styleindex.css' />
</head>
<body>
    <div id='tablewrapper'>
        <div id='tableheader'>
            <span class='details'>
            </span>
        </div>
        <h1>Clients</h1><br>


<table cellpadding='0' cellspacing='0' border='0' id='table' class='tinytable'>
            <thead>
                <tr>
                    <th class='nosort'><h3>ID</h3></th>
                    <th><h3>PC name</h3></th>
                    <th><h3>Confdir</h3></th>
                    <th><h3>IP</h3></th>
                    <th><h3>PORT</h3></th>
                    <th><h3>STATUS</h3></th>
                    <th><h3>UPDATE</h3></th>
                </tr>
            </thead>
            <tbody>

";


  
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", "$username", "$password");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
    $sql = 'SELECT * FROM clients ';
    foreach ($conn->query($sql) as $row) {

                echo "
                <tr>
                    
                    <td>".$row['id']."</td>
                    <td>".$row['name']."</td>
                    <td>".$row['confdir']."</td>
                    <td>".$row['ip']."</td>
                    <td>".$row['port']."</td>
                    <td>status</td>
                    <td><a href='http://".$row['ip'].":".$row['port']."/?script=updatecontainerstatus?name=all'>Update now</a></td>
                </tr>
                ";

                   

    }
    $conn = null;
  }
  catch(PDOException $err) {
    echo "Ошибка: не удается подключиться: " . $err->getMessage();
  }

 
echo "  
            </tbody>
        </table>
    </div>
</body>
</html>
";

?>

